# README #

This repository was create for the GGJ15 Paris at Isart Digital.

## Team ##

**Game Designers**

Douglas Alves

Florian Dufour

Nicolas Tézé

**Artists**

Benoit Onillon

Pierre Brouchet

**Sound Designer**

Yoan K

**Programmers**

Jérôme Beaune

Benjamin Baldacci

François Fournel